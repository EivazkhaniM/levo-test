import "./cards.scss";

import React, { Component } from "react";

class Cards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      visible: 3,
      error: false,
    };

    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
    this.setState((prev) => {
      return { visible: prev.visible + 3 };
    });
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => res.json())
      .then((res) => {
        this.setState({
          items: res,
        });
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          error: true,
        });
      });
  }
  render() {
    return (
      <React.Fragment>
        <div className="row" aria-live="polite">
          {this.state.items.slice(0, this.state.visible).map((item, index) => {
            return (
              <div
                className="col-md-4 d-flex align-items-stretch mb-4 fade-in"
                key={item.id}
              >
                <div className="card text-white bg-warning">
                  <div className="card-body d-flex flex-column">
                    <p className="small">{index + 1} Sep 2018</p>
                    <h5 className="mb-3">{item.title}</h5>
                    <p>{item.body}</p>
                    <div className="mt-auto">
                      <a
                        href="http://google.com.au"
                        className="btn btn-sm btn-outline-light text-uppercase"
                      >
                        Read More
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>

        {this.state.visible < this.state.items.length && (
          <div className="row my-2">
            <div className="col text-center">
              <button
                onClick={this.loadMore}
                type="button"
                className="btn btn-outline-danger text-uppercase load-more"
              >
                Load more
              </button>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default Cards;
