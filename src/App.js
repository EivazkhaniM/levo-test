import './App.scss';
import Cards from './Components/cards';

function App() {
    return (
        <div className="container my-5">
            <div className="row">
                <div className="col-md-8">
                    <h3 className="text-danger">Related Articles</h3>
                    <p className="my-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum congue tempor felis, vel consequat metus imperdiet eget. Praesent luctus ante quam, a aliquet mauris congue nec. Vestibulum congue tempor felis, vel consequat metus imperdiet ego.</p>
                </div>
            </div>
            <Cards />
        </div>
    );
}

export default App;
